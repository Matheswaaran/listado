import React from 'react';
import HomeStack from './app/Home';
import NearMeStack from './app/NearMe';
import SearchStack from './app/Search';
import FavouritesStack from './app/Favourites';
import { createBottomTabNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import About from "./app/About";

export default createBottomTabNavigator({
  Home: HomeStack,
  NearMe: NearMeStack,
  Search: SearchStack,
  Favourites: FavouritesStack,
  About: About,
},{
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let icon;
      switch (routeName){
        case 'Home':
          icon = focused ? 'home' : 'home-outline';
          break;
        case 'NearMe':
        icon = focused ? 'map-marker' : 'map-marker-outline';
          break;
        case 'Search':
          icon = 'magnify';
          break;
        case 'Favourites':
          icon = focused ? 'heart' : 'heart-outline';
          break;
        case 'About':
          icon = focused ? 'alert-circle' : 'alert-circle-outline';
          break;
      }
      return <Icon name={icon} size={25} color={tintColor} />;
    },
  }),
  tabBarOptions: {
    activeTintColor: '#E02222',
    inactiveTintColor: '#8E8E93',
    tabStyle: {
      
    },
  },
},
);