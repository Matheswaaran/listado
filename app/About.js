import { createStackNavigator } from "react-navigation";
import AboutAuthor from "./AboutAuthor";

export default createStackNavigator({
    AboutAuthor: {screen: AboutAuthor},
});