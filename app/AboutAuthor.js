import React from 'react';
import {View, Text, StyleSheet, Platform, ScrollView, Image, TouchableOpacity, Linking} from 'react-native';
import {GRAPHQL_API} from "./utils/URL";
import CreateAlert from "./utils/CreateAlert";
import FAIcon from "react-native-vector-icons/FontAwesome";
import HTMLView from 'react-native-htmlview';

class AboutAuthor extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        ...Platform.select({ ios: {title: "About Author"}, android: {headerLeft: (<Text style={styles.heading}>About Author</Text>)} }),
    });

    constructor(props) {
        super(props);
        this.state = { about_author: {}, about_app: {} };
    }

    onLoadApiData = async () => {
        try{
            let settings = {
                method: 'POST',
                headers: {
                    'deviceid': 'abc',
                    'Content-Type': 'application/json',
                    'cache-control': 'no-cache'
                },
                body: "{\"query\":\"{aboutAuthor{ id name image_url fb g_plus twitter instagram pintrest youtube } aboutApp{ id name title description}}\"}",
            };
            this.setState({refresh: true});
            const result = await fetch(GRAPHQL_API, settings);
            const response = await result.json();
            this.setState({ about_author: response.data.aboutAuthor[0], about_app: response.data.aboutApp[0] });
        }catch (e) {
            console.error(e);
        }
    };

    async componentDidMount() {
        try{
            this.onLoadApiData();
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        const { about_author, about_app } = this.state;
        return (
            <ScrollView style={{ backgroundColor: "#FFFFFF", flex: 1 }}>
                <View style={styles.body}>
                    <View style={styles.container}>
                        <View style={styles.image}>
                            <Image style={styles.imageView} source={{ uri: about_author.image_url }}/>
                        </View>
                        <View style={styles.texts}>
                            <Text style={styles.title}>{about_author.name}</Text>
                            <View style={styles.wrap}>
                                <TouchableOpacity onPress={() => Linking.openURL(about_author.fb)} style={styles.socialItem}>
                                    <FAIcon name="facebook-square" size={30} color="#475993"/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Linking.openURL(about_author.twitter)} style={styles.socialItem}>
                                    <FAIcon name="twitter-square" size={30} color="#65BBF2"/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Linking.openURL(about_author.instagram)} style={styles.socialItem}>
                                    <FAIcon name="instagram" size={30} color="#FF4500"/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Linking.openURL(about_author.pintrest)} style={styles.socialItem}>
                                    <FAIcon name="pinterest-square" size={30} color="#C9353D"/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Linking.openURL(about_author.g_plus)} style={styles.socialItem}>
                                    <FAIcon name="google-plus-square" size={30} color="#DD4B39"/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Linking.openURL(about_author.youtube)} style={styles.socialItem}>
                                    <FAIcon name="youtube-square" size={30} color="#F61C0D"/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={styles.description}>
                        {/*<Text style={styles.descInfo}>{about_app.description}</Text>*/}
                        <HTMLView value={about_app.description} style={styles.descInfo}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    heading: {
        padding: 16,
        color: '#E02222',
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: '900',
    },
    body:{
        flex: 1,
        backgroundColor: "#FFFFFF",
        flexDirection: 'column',
    },
    container: {
        backgroundColor: 'white',
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 10,
        marginTop:20,
    },
    image: {
        flex: 0.3,
        margin: 11,
        marginRight: 5,
        height: 95,
    },
    imageView: {
        flex: 1,
        borderRadius: 10,
    },
    texts: {
        flex: 0.7,
    },
    title: {
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontStyle: 'normal',
        fontWeight: 'bold',
        lineHeight: 21,
        fontSize: 16,
        marginTop: 20,
        marginLeft: 5,
        marginBottom: 2,
        color: '#000000',
    },
    wrap: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 20,
    },
    socialItem: {
        alignSelf: 'center',
        margin: 5,
        marginTop: 0,
    },
    descInfo: {
        margin: 16,
        marginTop: 8,
        marginBottom: 20,
    },
});

export default AboutAuthor;