import React from "react";
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, ImageBackground } from "react-native";

class CollectionItem extends React.Component{
    constructor(props){
        super(props);
    }
    
    render(){
        const {collectionItem, navigate} = this.props;
        return(
            <View style={styles.body}>
                <ImageBackground source={{ uri: collectionItem.thumb_image }} style={styles.collectionImage} imageStyle={{ borderRadius: 10, }}>
                    <TouchableOpacity onPress={navigate.bind(this, collectionItem.id, collectionItem.name, collectionItem.description, collectionItem.list_count)}>
                        <View style={styles.viewButton}>
                            <Text style={{ color: '#FF7800', }}>View</Text>
                        </View>
                    </TouchableOpacity>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        margin: 10,
        marginBottom: 0,
        height: '100%',
        width: (Dimensions.get('window').width - 32) * 0.5,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    collectionImage: {
        height: '100%',
        width: '100%',
        flex: 1,
        resizeMode: 'cover',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    viewButton: {
        height: 27,
        width: 95,
        backgroundColor: '#FFFFFF',
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
    },
});

export default CollectionItem;