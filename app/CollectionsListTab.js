import React from "react";
import {StyleSheet, TouchableOpacity, View, ScrollView, RefreshControl, Text, Platform, Dimensions} from "react-native";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import ListItem from "./ListItem";
import {GRAPHQL_API, SEARCHFILTER_API} from "./utils/URL";
import CreateAlert from "./utils/CreateAlert";
import { ContextConsumer } from "./Context/Provider";
import { region } from "./utils/Data";

class CollectionsListTab extends React.Component{
    constructor(props){
        super(props);
        this.state = {api_value: [], refresh: false, readMore: false, region: {latitude: 43.76786386109126, longitude: 11.253059562295675, latitudeDelta: 0.005088616980401639, longitudeDelta: 0.004248954355718482}};
    }

    loadApiData = async () => {
        try{
            const collections_id = this.props.navigation.getParam("collections_id", "");
            let settings = {
                method: 'POST',
                headers: {
                    'deviceid': 'abc',
                    'Content-Type': 'application/json',
                    'cache-control': 'no-cache'
                },
                body: "{\"query\":\"{list(collections_id:"+ `${collections_id}` +"){ id thumb_image name cost_categorization display_name byline lat lang phone_no }}\"}",
            };
            this.setState({refresh: true});
            const result = await fetch(GRAPHQL_API, settings);
            const response = await result.json();
            this.setState({api_value: response.data.list, refresh: false});
        }
        catch(e){
            console.error(e);
            // CreateAlert("", error);
        }
    };

    async componentDidMount() {
        const { navigation } = this.props;
        const collections_id = this.props.navigation.getParam("collections_id", "");
        try {
            this.setState({ region:region[collections_id] });
            const res = await this.loadApiData();
            navigation.setParams({ loadFilter: this.navigationSetState, removeFilter: this.removeFilterHandler });
        }catch (e) {
            console.error(e);
            // CreateAlert("", e);
        }
    }

    navigationSetState = async (filterVal) => {
        const collections_id = this.props.navigation.getParam("collections_id", "");
        const { latitude, longitude } = this.state.region;
        const { cost_catagory, distance, cuisinename, typename } = filterVal;
        try{
            let settings = {
                method: 'GET',
                headers: { 'deviceid': 'abc', 'Content-Type': 'application/json'},
            };
            this.setState({ refresh: true });
            const result = await fetch(`${SEARCHFILTER_API}?collections_id=${collections_id}&cost_categorization=${cost_catagory}&cuisinename=${cuisinename}&typename=${typename}&lat=${latitude}&lang=${longitude}&distance=${distance}`, settings);
            console.log(`${SEARCHFILTER_API}?collections_id=${collections_id}&cost_categorization=${cost_catagory}&cuisinename=${cuisinename}&typename=${typename}&lat=${latitude}&lang=${longitude}&distance=${distance}`);
            const response = await result.json();
            const apiData = response.lists.map(listitem => {
                const {id, name, byline, display_name, cost_categorization, thumb_image, lat, lang, phone_no} = listitem;
                const stringID = id.toString();
                return {id: stringID, name, byline, display_name, cost_categorization, thumb_image, lat, lang, phone_no};
            });
            this.setState({api_value: apiData, refresh: false});
        }
        catch(e){
            console.error(e);
            // CreateAlert("", e);
        }
    };

    _onScrollViewRefresh = async () => {
        try {
            const {contextState} = this.props;
            this.setState({refresh: true});
            if (contextState.isFilterApplicableHandler()) {
                await this.navigationSetState(contextState.filterData);
            } else {
                await this.loadApiData();
            }
            this.setState({refresh: false});
        }catch(e){
            console.error(e);
            // CreateAlert("", e);
        }
    };

    listItemPressHandler = (item_id, item_name) => {
        this.props.navigation.navigate('DetailedView', {"item_id": item_id, "name": item_name});
    };

    removeFilterHandler = async () => {
        try {
            this.setState({ filterData: {} });
            await this.loadApiData();
        }catch (e) {

        }
    };

    render(){
        const { navigation } = this.props;
        return (
            <View style={{ backgroundColor: "#FFFFFF", flex: 1 }}>
                <ScrollView style={styles.scrollview} refreshControl= {
                    <RefreshControl refreshing={this.state.refresh} onRefresh={this._onScrollViewRefresh} />
                }>
                    <View style={styles.description}>
                        <View style={styles.containerRow}>
                            <Text style={styles.descTitle}>{navigation.getParam("collection_name", "")}</Text>
                            <Text style={styles.descOpen}>{navigation.getParam("collection_count", "") + " Places"}</Text>
                        </View>
                        <Text style={styles.descInfo} ellipsizeMode={'tail'} numberOfLines={this.state.readMore ? 10000 : 6}>{navigation.getParam("collection_desc", "")}</Text>
                        <TouchableOpacity onPress={() => this.setState({readMore: !this.state.readMore})}>
                            <Text style={styles.readMore}>{this.state.readMore ? 'Show Less' : 'Read More'}</Text>
                        </TouchableOpacity>
                    </View>
                    {this.state.api_value.map((item, index) => (
                        <TouchableOpacity onPress={this.listItemPressHandler.bind(this, item.id, item.name)} key={item.id}>
                            <ListItem key={item.id} item={item} />
                        </TouchableOpacity>
                    ))}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#FFFFFF',
        flex: 1,
    },
    body: {
        marginBottom: 15,
        marginTop: 10,
    },
    settings: {
        marginRight: 20,
    },
    containerRow: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    description: {
        backgroundColor: '#FFFFFF',
    },
    descTitle: {
        flex: 0.7,
        color: '#828282',
        marginLeft: 16,
        marginTop: 16,
    },
    descOpen: {
        flex: 0.25,
        color: '#828282',
        marginRight: -5,
        marginTop: 16,
        textTransform: 'capitalize',
        alignSelf: 'flex-end',
    },
    descInfo: {
        margin: 16,
        marginTop: 8,
        marginBottom: 5,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontStyle: 'normal',
    },
    readMore: {
        marginLeft: 16,
        marginBottom: 16,
        color: '#000000',
        fontWeight: 'bold',
    },
    descInfoWeb: {
        alignSelf: 'stretch',
        width: Dimensions.get('window').width,
        marginLeft: 16,
        marginRight: 16,
        marginBottom: 5,
        marginTop: 5,
    },
});

class ListConsumer extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        headerLeft:
            (<TouchableOpacity style={{ marginLeft: 20 }} onPress={() => navigation.goBack()}>
                <MaterialIcon name="chevron-left" size={23} color='#E02222' />
            </TouchableOpacity>),
        title: navigation.getParam("collection_name", ""),
        headerRight:
            (<TouchableOpacity style={styles.settings} onPress={() => navigation.navigate('ListFilter', { applyFilter: navigation.state.params.loadFilter, removeFilter: navigation.state.params.removeFilter, filterValues: navigation.state.params.filterData })}>
                <MaterialIcon name="filter-list" size={23} color='#E02222' />
            </TouchableOpacity>)
    });

    render() {
        return (
            <ContextConsumer>
                {(contextState) => (
                    <CollectionsListTab navigation={this.props.navigation} contextState={contextState}/>
                )}
            </ContextConsumer>
        );
    }
}

export default ListConsumer;