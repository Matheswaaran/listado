import React from "react";
import { StyleSheet, View, RefreshControl, TouchableOpacity, Text, Dimensions, ScrollView, Platform } from "react-native";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MCIcon from "react-native-vector-icons/MaterialCommunityIcons";
import SplashScreen from 'react-native-splash-screen';
import CollectionItem from "./CollectionItem";
import { GRAPHQL_API } from "./utils/URL";
import CreateAlert from "./utils/CreateAlert";

class CollectionsTab extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        ...Platform.select({ ios: {title: "Home"}, android: {headerLeft: (<Text style={styles.heading}>Home</Text>)} }),
        headerRight: 
            (<TouchableOpacity style={styles.settings} title="Set" onPress={() => navigation.navigate("Settings")}>
                <MCIcon name="settings-outline" size={23} color='#579041' />
            </TouchableOpacity>)
      });

    constructor(props){
        super(props);
        this.state = {api_value: [], refresh: false};
    }

    loadApiData = async () => {
        try{
            let settings = {
                method: 'POST',
                headers: {
                    'deviceid': 'abc',
                    'Content-Type': 'application/json',
                    'cache-control': 'no-cache'
                },
                body: "{\"query\":\"{listCollections{id is_free thumb_image name updatedAt description list_count}}\"}",
            };
            this.setState({refresh: true});
            const result = await fetch(GRAPHQL_API, settings);
            const response = await result.json();
            this.setState({api_value: response.data.listCollections, refresh: false});
        } catch(e){
            CreateAlert("", JSON.stringify(e));
        }
    };

    async componentDidMount() {
        try {
            await this.loadApiData();
            SplashScreen.hide();
        }catch (e) {
            CreateAlert("", JSON.stringify(e));
        }
    }

    _onScrollViewRefresh = async () => {
        try {
            this.setState({refresh: true});
            await this.loadApiData();
            this.setState({refresh: false});
        }catch (e) {
            CreateAlert("", JSON.stringify(e));
        }
    };

    onCollectionClickHandler = (collections_id, collection_name, collection_desc, collection_count) => {
        this.props.navigation.navigate('CollectionsList', {collections_id, collection_name, collection_desc, collection_count});
    };

    render(){
        return (
            <ScrollView style={{ backgroundColor: '#FFFFFF' }} refreshControl={
                <RefreshControl refreshing={this.state.refresh} onRefresh={this._onScrollViewRefresh}/>
            }>
                <View style={styles.body}>
                    {this.state.api_value.map((collectionItem) => (
                        <View style={styles.collectionItem} key={collectionItem.id}>
                            <CollectionItem collectionItem={collectionItem} navigate={this.onCollectionClickHandler}/>
                        </View>
                    ))}
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    heading: {
        padding: 16,
        color: '#E02222',
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: '900',
    },
    body: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginBottom: 16,
    },
    settings: {
        marginRight: 20,
    },
    collectionItem: {
        flex: 0.5,
        height: Dimensions.get('window').height * 0.25,
        minWidth: Dimensions.get('window').width * 0.5,
        flexGrow: 0,
        flexShrink: 1,
        flexBasis: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default CollectionsTab;