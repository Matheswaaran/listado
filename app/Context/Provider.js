import React from 'react';
import { AsyncStorage, Alert } from 'react-native';
import { ASYNC_STORAGE_NAME } from "../utils/URL";

const ReactContext = React.createContext();
const ContextConsumer = ReactContext.Consumer;

class ContextProvider extends React.Component{
    constructor(props) {
        super(props);
        this.state = { favourites: [], filterData: { cost_catagory: "", distance: "", cuisinename: "", typename: "" } };
    }

    setFavourites = async (itemData) => {
        try{
            const data = this.state.favourites.map(x => x);
            if (data.filter(filter_item => filter_item.id === itemData.id).length === 0){
                data.push(itemData);
            } else {
                data.splice(data.indexOf(itemData), 1);
            }
            this.setState({ favourites: data });
            let favs = JSON.stringify(data);
            console.log(data);
            const result = await AsyncStorage.setItem(ASYNC_STORAGE_NAME, favs);
        } catch (e) {
            console.error(e);
        }
    };

    getFavourites = async () => {
        try{
            const data = await AsyncStorage.getItem(ASYNC_STORAGE_NAME);
            return JSON.parse(data);
        }catch (e) {
            console.error(e)
        }
    };

    isFavorite = (item_id) => {
        try {
            const data = this.state.favourites.map(x => x);
            return data.filter(filter_item => filter_item.id === item_id).length !== 0;
        }catch (e) {
            console.error(e)
        }
    };

    async componentDidMount() {
        try {
            if(await typeof AsyncStorage.getItem(ASYNC_STORAGE_NAME) === 'undefined' || await AsyncStorage.getItem(ASYNC_STORAGE_NAME) === null){
                AsyncStorage.setItem(ASYNC_STORAGE_NAME, JSON.stringify([]));
            }
            const data = await AsyncStorage.getItem(ASYNC_STORAGE_NAME);
            this.setState({ favourites: JSON.parse(data) });
        } catch (e) {
            console.error(e);
        }
    }

    //FILTER FUNCTIONS
    isFilterApplicableHandler = () => {
        const { distance, cost_catagory, cuisinename,typename } = this.state.filterData;
        return cost_catagory !== "" || distance !== "" || cuisinename.length !== 0 || typename.length !== 0;
    };

    onSelectCostCategory = (value) => {
        let { filterData } = this.state;
        if (filterData.cost_catagory === value){
            filterData.cost_catagory = "";
        } else {
            filterData.cost_catagory = value;
        }
        this.setState({ filterData: filterData });
    };

    onGetCostCategory = () => {
        return this.state.filterData.cost_catagory;
    };

    onSelectDistance = (value) => {
        let { filterData } = this.state;
        if (filterData.distance === value){
            filterData.distance = "";
        } else {
            filterData.distance = value;
        }
        this.setState({ filterData: filterData });
    };

    onGetDistance = () => {
        return this.state.filterData.distance;
    };

    onClearFilterHandler = () => {
        this.setState({ filterData: { cost_catagory: "", distance: "", cuisinename: "", typename: "" } });
    };

    onGetCusinesHandler = () => {
        return this.state.filterData.cuisinename;
    };

    onCusinesDoneClickHandler = (cusines) => {
        this.setState({ filterData: { ...this.state.filterData, cuisinename: cusines.join(',') }});
    };

    onGetTypesHandler = () => {
        return this.state.filterData.typename;
    };

    onTypesDoneClickHandler = (types) => {
        this.setState({ filterData: { ...this.state.filterData, typename: types.join(',') }});
    };

    render() {
        let initialValue = {
            ...this.state,
            isFilterApplicableHandler: this.isFilterApplicableHandler,
            setFavourites: this.setFavourites,
            getFavourites: this.getFavourites,
            isFavorite: this.isFavorite,
            refreshFavourites: this.refreshFavourites,
            onSelectCostCategory: this.onSelectCostCategory,
            onGetCostCategory: this.onGetCostCategory,
            onSelectDistance: this.onSelectDistance,
            onGetDistance: this.onGetDistance,
            onGetCusinesHandler: this.onGetCusinesHandler,
            onCusinesDoneClickHandler: this.onCusinesDoneClickHandler,
            onGetTypesHandler: this.onGetTypesHandler,
            onTypesDoneClickHandler: this.onTypesDoneClickHandler,
            onApplyFilterHandler: this.onApplyFilterHandler,
            onClearFilterHandler: this.onClearFilterHandler,
        };
        return (
            <ReactContext.Provider value={initialValue}>
                {this.props.children}
            </ReactContext.Provider>
        );
    }
}

export {ContextProvider, ContextConsumer}