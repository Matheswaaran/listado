import React from "react";
import {Platform,View,StyleSheet,TouchableOpacity,ScrollView,RefreshControl,Image,Animated,Dimensions,Linking,Text,Share} from "react-native";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FAIcon from "react-native-vector-icons/FontAwesome";
import { GRAPHQL_API, NO_IMAGE_AVAILABLE } from "./utils/URL";
import { ContextConsumer } from "./Context/Provider";
import CreateAlert from './utils/CreateAlert';

class DetailedViewTab extends React.Component{

    constructor(props){
        super(props);
        this.state = {readMore: false, noOfLines: 6, itemData: {description:"", images: [], cuisines: [], category_ideal_fors: [], type_dietary_needs: [], recommended: [], ambiences: []}, refresh: false};
    }
    
    static navigationOptions = ({ navigation }) => ({
        headerLeft:
            (<TouchableOpacity style={{ marginLeft: 20 }} onPress={() => navigation.goBack()}>
                <MaterialIcon name="chevron-left" size={23} color='#E02222' />
            </TouchableOpacity>),
        title: navigation.getParam("name", ""),
    });

    loadApiData = async () => {
        const item_id = this.props.navigation.getParam("item_id", "");
        let settings = {
            method: 'POST',
            headers: {
                'deviceid': 'abc',
                'Content-Type': 'application/json',
                'cache-control': 'no-cache'
            },
            body: "{\"query\":\"{list(id:"+ `${item_id}` +"){id name byline thumb_image display_name images{ url } description timing timing_note phone_no address_line1 address_line2 website cuisines{name} category_ideal_fors{name} type_dietary_needs{name} recommended ambiences{name} cost_categorization cost_two fb twitter instagram pintrest g_plus youtube lat lang}}\"}",
        };
        this.setState({refresh: true});
        const result = await fetch(GRAPHQL_API, settings);
        const response = await result.json();
        const apiData = response.data.list[0];
        this.setState({itemData: apiData, refresh: false});
    };

    shareIconClickHandler = () => {
        Share.share({
            title: this.state.itemData.name,
            message: `I found ${this.state.itemData.name} Listado app. Check it out at http://www.listado.co.`,
        });
    };

    onLocationIconClickHandler = () => {
        const {lat, lang, name} = this.state.itemData;
        const URL = Platform.select({
            android: `geo:0,0?q=${lat},${lang}`,
            ios: `maps:0,0?=q=${name}@${lat},${lang}`,
        });
        Linking.openURL(URL);
    };

    async componentDidMount() {
        try {
            const res = await this.loadApiData();
        }catch (e) {
            console.error(e)
        }
    }

    _onScrollViewRefresh = async () => {
        try {
            this.setState({refresh: true});
            const res = await this.loadApiData();
            this.setState({refresh: false});
        }catch (e) {
            console.error(e)
        }
    };

    render(){
        const { itemData } = this.state;
        const {id, name, byline, thumb_image, display_name} = this.state.itemData;
        const item = {id, name, byline, thumb_image, display_name};
        return (
            <ScrollView style={{ backgroundColor: '#FFFFFF' }} refreshControl={
                <RefreshControl refreshing={this.state.refresh} onRefresh={this._onScrollViewRefresh}/>
            }>
                <View style={styles.body}>
                    <View style={styles.containerCol}>
                        <Animated.View style={styles.imageIndex}>
                            <Text>1/{itemData.images.length}</Text>
                        </Animated.View>
                        {itemData.images.length === 0 ? (
                            <Image style={styles.intoImage} source={{ uri: NO_IMAGE_AVAILABLE}}/>
                            ) : (
                            <ScrollView
                                horizontal={true}
                                pagingEnabled={true}
                                scrollsToTop={false}
                                scrollEventThrottle={100}
                                removeClippedSubviews={true}
                                automaticallyAdjustContentInsets={false}
                                directionalLockEnabled={true}>
                                {itemData.images.map((image, index) => (
                                    <View key={index}>
                                        <Image style={styles.intoImage} source={{ uri: image.url }}/>
                                    </View>
                                ))}
                            </ScrollView>
                        )}
                        <View style={ styles.horizontalLine } />
                        <View style={styles.containerRow}>
                            <View style={styles.imageIcons}>
                                <ContextConsumer>
                                    {( contextState ) => (
                                        <TouchableOpacity onPress={contextState.setFavourites.bind(this, item)} style={{ alignSelf: 'center', margin: 15 }}>
                                            <MaterialIcon name={contextState.isFavorite(itemData.id) ? "favorite" : "favorite-border"} size={25} color="#E02222"/>
                                        </TouchableOpacity>
                                    )}
                                </ContextConsumer>
                            </View>
                            <View style={styles.imageIcons}>
                                <TouchableOpacity onPress={() => Linking.openURL(`tel:${itemData.phone_no}`)} style={{ alignSelf: 'center', margin: 15 }}>
                                    <MaterialIcon name="call" size={25} color="#007D35"/>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.imageIcons}>
                                <TouchableOpacity onPress={this.onLocationIconClickHandler} style={{ alignSelf: 'center', margin: 15 }}>
                                    <MaterialIcon name="location-on" size={25} color="#FF7800"/>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.imageIcons}>
                                <TouchableOpacity onPress={this.shareIconClickHandler} style={{ alignSelf: 'center', margin: 15 }}>
                                    <MaterialIcon name="share" size={25} color="#006DFF"/>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={ styles.horizontalLine } />
                        <View style={styles.description}>
                            <View style={styles.containerRow}>
                                <Text style={styles.descTitle}>About {itemData.name}</Text>
                                <Text style={styles.descOpen}>Open Now</Text>
                            </View>
                            <Text style={styles.descInfo} ellipsizeMode={'tail'} numberOfLines={this.state.readMore ? 10000 : 6}>{itemData.description}</Text>
                            <TouchableOpacity onPress={() => this.setState({readMore: !this.state.readMore})}>
                                <Text style={styles.readMore}>{this.state.readMore ? 'Show Less' : 'Read More'}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={ styles.horizontalLine } />
                        <View style={styles.infoBlock}>
                            <Text style={styles.infoTitle} >Timing</Text>
                            <Text style={styles.infoDataBlack} >{itemData.timing_note}</Text>
                            <Text style={styles.infoTitle} >Phone</Text>
                            <Text style={styles.infoDataBlue} onPress={() => Linking.openURL(`tel:${itemData.phone_no}`)}>{itemData.phone_no}</Text>
                            <Text style={styles.infoTitle} >Address</Text>
                            <Text style={styles.infoDataBlack}>{itemData.address_line1}</Text>
                            <Text style={styles.infoTitle} >Website</Text>
                            <Text style={styles.infoDataBlue} onPress={() => Linking.openURL(itemData.website)}>{itemData.website}</Text>
                        </View>
                        <View style={ styles.horizontalLine } />
                        <View style={styles.infoBlock}>
                            {itemData.cuisines === null || itemData.cuisines.length === 0 ? null : (
                                <View>
                                    <Text style={styles.infoTitle} >Cuisine</Text>
                                    <Text style={styles.infoDataBlack} >{itemData.cuisines.map(element => element.name+"; ")}</Text>
                                </View>
                            )}
                            {itemData.category_ideal_fors === null || itemData.category_ideal_fors.length === 0 ? null : (
                                <View>
                                    <Text style={styles.infoTitle} >Ideal For</Text>
                                    <Text style={styles.infoDataBlack} >{itemData.category_ideal_fors.map(element => element.name+"; ")}</Text>
                                </View>
                            )}
                            {itemData.type_dietary_needs === null || itemData.type_dietary_needs.length === 0 ? null : (
                                <View>
                                    <Text style={styles.infoTitle} >Dietary need</Text>
                                    <Text style={styles.infoDataBlack} >{itemData.type_dietary_needs.map(element => element.name+"; ")}</Text>
                                </View>
                            )}
                            {itemData.recommended === null || itemData.recommended.length === 0 ? null : (
                                <View>
                                    <Text style={styles.infoTitle} >Must try</Text>
                                    <Text style={styles.infoDataBlack} >{itemData.recommended.map(element => element.name+"; ")}</Text>
                                </View>
                            )}
                            {itemData.ambiences.length === 0 || itemData.ambiences === null ? null : (
                                <View>
                                    <Text style={styles.infoTitle} >Amenities</Text>
                                    <Text style={styles.infoDataBlack} >{itemData.ambiences.map(element => element.name+"; ")}</Text>
                                </View>
                            )}
                        </View>
                        <View style={ styles.horizontalLine } />
                        <View style={styles.infoBlock}>
                            <Text style={styles.infoTitle} >Cost</Text>
                            <Text style={styles.infoDataBlack} >{itemData.cost_categorization}, {itemData.cost_two}</Text>
                        </View>
                        <View style={ styles.horizontalLine } />
                        <View style={styles.infoBlock}>
                            <Text style={styles.infoTitle} >Social</Text>
                            <View style={styles.containerSocial}>
                                {itemData.fb === "" || itemData.fb === null ? null : (
                                    <TouchableOpacity onPress={() => Linking.openURL(itemData.fb)} style={styles.socialItem}>
                                        <FAIcon name="facebook-square" size={30} color="#475993"/>
                                    </TouchableOpacity>
                                )}
                                {itemData.twitter === "" || itemData.twitter === null ? null : (
                                    <TouchableOpacity onPress={() => Linking.openURL(itemData.twitter)} style={styles.socialItem}>
                                        <FAIcon name="twitter-square" size={30} color="#65BBF2"/>
                                    </TouchableOpacity>
                                )}
                                {itemData.instagram === "" || itemData.instagram === null ? null : (
                                    <TouchableOpacity onPress={() => Linking.openURL(itemData.instagram)} style={styles.socialItem}>
                                        <FAIcon name="instagram" size={30} color="#FF4500"/>
                                    </TouchableOpacity>
                                )}
                                {itemData.pintrest === "" || itemData.pintrest === null ? null : (
                                    <TouchableOpacity onPress={() => Linking.openURL(itemData.pintrest)} style={styles.socialItem}>
                                        <FAIcon name="pinterest-square" size={30} color="#C9353D"/>
                                    </TouchableOpacity>
                                )}
                                {itemData.g_plus === "" || itemData.g_plus === null ? null : (
                                    <TouchableOpacity onPress={() => Linking.openURL(itemData.g_plus)} style={styles.socialItem}>
                                        <FAIcon name="google-plus-square" size={30} color="#DD4B39"/>
                                    </TouchableOpacity>
                                )}
                                {itemData.youtube === "" || itemData.youtube === null ? null : (
                                    <TouchableOpacity onPress={() => Linking.openURL(itemData.youtube)} style={styles.socialItem}>
                                        <FAIcon name="youtube-play" size={30} color="#F61C0D"/>
                                    </TouchableOpacity>
                                )}
                            </View>
                        </View>
                        <View style={ styles.horizontalLine } />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: '#FFFFFF',
        paddingBottom: 16,
    },
    containerCol: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
    },
    containerRow: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    intoImage: { 
        flex: 1,
        alignSelf: 'stretch',
        width: Dimensions.get('window').width,
        height: 225,
        overflow: 'visible',
    },
    imageIndex:{
        position: "absolute",
        backgroundColor: '#FFFFFF',
        width: 40,
        right: 16,
        top: 16,
        overflow: 'visible',
        alignItems: 'center',
        zIndex: 5,
    },
    imageIcons: {
        flex: 0.25,
        margin: 14,
        borderRadius: 10,
        ...Platform.select({ ios: { borderColor: '#ddd', borderWidth: 1, } }),
        elevation: 2,
    },
    horizontalLine: {
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E0',
        marginLeft: 17,
        marginRight: 17,
    },
    description: {
        backgroundColor: '#f2faff',
    },
    descTitle: {
        flex: 0.8,
        color: '#A5A5A5',
        marginLeft: 16,
        marginTop: 16,
    },
    descOpen: {
        flex: 0.2,
        color: '#219653',
        marginRight: 16,
        marginTop: 16,
        textTransform: 'capitalize',
    },
    descInfo: {
        margin: 16,
        marginTop: 8,
        marginBottom: 5,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontStyle: 'normal',
    },
    readMore: {
        marginLeft: 16,
        marginBottom: 16,
        color: '#000000',
        fontWeight: 'bold',
    },
    descInfoWeb: {
        alignSelf: 'stretch',
        width: Dimensions.get('window').width,
        marginLeft: 16,
        marginRight: 16,
        marginBottom: 5,
        marginTop: 5, 
    },
    infoBlock: {
        marginTop: 16,
    },
    infoTitle: {
        marginLeft: 16,
        marginBottom: 5,
        color: '#828282',
        fontSize: 12,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
    },
    infoDataBlack: {
        marginLeft: 16,
        marginBottom: 16,
        color: '#333333',
        fontSize: 14,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
    },
    infoDataBlue: {
        marginLeft: 16,
        marginBottom: 16,
        color: '#2F80ED',
        fontSize: 14,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
    },
    containerSocial: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    socialItem: {
        alignSelf: 'center',
        margin: 16,
        marginTop: 0,
    },
});

export default DetailedViewTab;