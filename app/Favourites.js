import { createStackNavigator } from "react-navigation";
import FavouritesList from "./FavouritesList";
import DetailedViewTab from "./DetailedViewTab";

export default createStackNavigator({
    FavouritesList: {screen: FavouritesList},
    FavouritesDetailView: {screen: DetailedViewTab},
});