import React from "react";
import { StyleSheet,  Text,  View,  ScrollView,  RefreshControl,  TouchableOpacity,  Platform } from "react-native";
import ListItem from "./ListItem";
import { ContextConsumer } from "./Context/Provider";
import CreateAlert from "./utils/CreateAlert";

class FavouritesList extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        ...Platform.select({ ios: {title: "Favourites"}, android: {headerLeft: (<Text style={styles.heading}>Favourites</Text>)} }),
    });

    constructor(props){
        super(props);
        this.state = {refresh: false};
    }

    listItemPressHandler(item_id, item_name){
        this.props.navigation.navigate('FavouritesDetailView', {"item_id": item_id, name: item_name});
    }

    render(){
        return(
            <ContextConsumer>
                {( contextState ) => (
                    <View style={styles.body}>
                        <ScrollView style={styles.scrollview}>
                            {contextState.favourites.map((item, index) => (
                                <TouchableOpacity onPress={this.listItemPressHandler.bind(this, item.id, item.name)} key={item.id}>
                                    <ListItem key={item.id} item={item} />
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                    </View>
                )}
            </ContextConsumer>
        );
    }
}

const styles = StyleSheet.create({
    heading: {
        padding: 16,
        color: '#E02222',
        // fontFamily: 'Lato',
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: '900',
    },
    body: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        paddingTop: 10,
        paddingBottom: 15,
    },
    settings: {
        marginRight: 20,
    },
});

export default FavouritesList;