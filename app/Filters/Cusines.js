import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, CheckBox, ScrollView} from 'react-native';
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import {GET_CUSINE_API} from "../utils/URL";
import CreateAlert from "../utils/CreateAlert";
import { ContextConsumer } from "../Context/Provider";

class Cusines extends React.Component{

    constructor(props) {
        super(props);
        this.state = {api_value: [], selected: []};
    }

    onLoadApiData = async () => {
        try{
            let settings = {
                method: 'GET',
                headers: { 'deviceid': 'abc', 'Content-Type': 'application/json'},
            };
            const result = await fetch(GET_CUSINE_API, settings);
            const response = await result.json();
            const res = response.cuisnes.map(item => {
                item.selected = false;
                return item;
            });
            this.setState({api_value: res});
        }
        catch(e){
            CreateAlert("", e);
        }
    };

    async componentDidMount() {
        try {
            const res = await this.onLoadApiData();
            this.props.navigation.setParams({ doneClick: this.onDoneBtnClickHandler });
            const oldData = this.props.contextState.onGetCusinesHandler();
            if (oldData !== "" || oldData.length !== 0){
                this.setState({ selected: oldData.split(",") });
            }
        }catch(e){
            // CreateAlert("", e);
        }
    }

    onDoneBtnClickHandler = () => {
        this.props.contextState.onCusinesDoneClickHandler(this.state.selected);
        this.props.navigation.navigate("ListFilter");
    };

    onSelectHandler = (name) => {
        let sel = this.state.selected.map(x => x);
        if (sel.includes(name)){
            sel.splice(sel.indexOf(name), 1);
        } else {
            sel.push(name);
        }
        this.setState({ selected: sel });
    };

    render() {
        return (
            <ScrollView style={styles.body}>
                <View style={styles.body}>
                    {this.state.api_value.map((item, index) => (
                        <View style={styles.containerRow} key={index}>
                            <Text style={styles.textValue} >{item.name}</Text>
                            <CheckBox style={styles.checkBox}
                                      value={this.state.selected.includes(item.name)}
                                      onChange={this.onSelectHandler.bind(this, item.name)} />
                        </View>
                    ))}
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    doneBtn: {
        color: "#579041",
        marginRight: 10,
        fontSize: 18,
    },
    containerRow:{
        display: "flex",
        flexWrap: "wrap",
        flexDirection: "row",
    },
    textValue: {
        flex: 0.9,
        fontSize: 18,
        margin: 16,
    },
    checkBox: {
        flex: 0.1,
        margin: 16,
        height: 20,
    },
});

class CusinesConsumer extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        headerLeft:
            (<TouchableOpacity style={{ marginLeft: 20 }} onPress={() => navigation.goBack()}>
                <MaterialIcon name="close" size={23} color='#E02222' />
            </TouchableOpacity>),
        title: "Cusines",
        headerRight:
            (<TouchableOpacity onPress={() => navigation.state.params.doneClick()}>
                <Text style={styles.doneBtn}>Done</Text>
            </TouchableOpacity>),
    });

    render() {
        return (
            <ContextConsumer>
                {( contextState ) => (
                    <Cusines contextState={contextState} navigation={this.props.navigation}/>
                )}
            </ContextConsumer>
        );
    }
}

export default CusinesConsumer;