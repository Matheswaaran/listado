import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ScrollView, Button} from 'react-native';
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import CreateAlert from "../utils/CreateAlert";
import { ContextConsumer } from "../Context/Provider";

class ListFilter extends React.Component{

    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        // console.log(this.props);
    }

    onTypesClickHandler = () => {
        this.props.navigation.navigate("DietaryNeeds");
    };

    onCusinesClickHandler = () => {
        this.props.navigation.navigate("Cusines");
    };

    onApplyFilterHandler = () => {
        const { navigation, contextState } = this.props;
        navigation.state.params.applyFilter(contextState.filterData);
        navigation.navigate("CollectionsList");
    };

    onClearFilterHandler = () => {
        const { navigation, contextState } = this.props;
        contextState.onClearFilterHandler();
        navigation.state.params.removeFilter();
        navigation.navigate("CollectionsList");
    };

    render() {
        return (
            <ContextConsumer>
                {( contextState ) => (
                    <ScrollView style={{ backgroundColor: "#FFFFFF", flex: 1, }}>
                        <View style={styles.body}>
                            <View style={styles.container}>
                                <Text style={styles.title}>PREFERENCES</Text>
                                <TouchableOpacity onPress={this.onTypesClickHandler}>
                                    <View style={styles.listItem}>
                                        <Text style={{fontSize: 16, flex: 0.9 }} >Dietary Needs</Text>
                                        <MaterialIcon style={{ flex: 0.1 }} name="chevron-right" size={23} color='#E02222' />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.onCusinesClickHandler}>
                                    <View style={styles.listItem}>
                                        <Text style={{fontSize: 16, flex: 0.9 }} >Cusines</Text>
                                        <MaterialIcon style={{ flex: 0.1 }} name="chevron-right" size={23} color='#E02222' />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.horizontalLine} />
                            <View style={styles.container}>
                                <Text style={styles.title}>COST</Text>
                                    <View style={styles.containerRow}>
                                        <TouchableOpacity onPress={contextState.onSelectCostCategory.bind(this, "Inexpensive")} style={contextState.onGetCostCategory() === "Inexpensive" ? styles.selectedCostCatagory : styles.costCatagory}>
                                            <Text style={contextState.onGetCostCategory() === "Inexpensive" ? {color: "#FFFFFF"} : {color: "#000000"}} >$</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={contextState.onSelectCostCategory.bind(this, "Moderate")} style={contextState.onGetCostCategory() === "Moderate" ? styles.selectedCostCatagory : styles.costCatagory}>
                                            <Text style={contextState.onGetCostCategory() === "Moderate" ? {color: "#FFFFFF"} : {color: "#000000"}} >$$</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={contextState.onSelectCostCategory.bind(this, "Expensive")} style={contextState.onGetCostCategory() === "Expensive" ? styles.selectedCostCatagory : styles.costCatagory}>
                                            <Text style={contextState.onGetCostCategory() === "Expensive" ? {color: "#FFFFFF"} : {color: "#000000"}} >$$$</Text>
                                        </TouchableOpacity>
                                    </View>
                            </View>
                            <View style={styles.horizontalLine} />
                            <View style={styles.container}>
                                <Text style={styles.title}>DISTANCE FROM ME</Text>
                                <View style={styles.containerRow}>
                                    <TouchableOpacity onPress={contextState.onSelectDistance.bind(this, "1")} style={contextState.onGetDistance() === "1" ? styles.selectedDistance : styles.distance}>
                                        <Text style={contextState.onGetDistance() === "1" ? {color: "#FFFFFF"} : {color: "#000000"}} >{"< 1km"}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={contextState.onSelectDistance.bind(this, "2")} style={contextState.onGetDistance() === "2" ? styles.selectedDistance : styles.distance}>
                                        <Text style={contextState.onGetDistance() === "2" ? {color: "#FFFFFF"} : {color: "#000000"}} >{"< 2km"}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={contextState.onSelectDistance.bind(this, "5")} style={contextState.onGetDistance() === "5" ? styles.selectedDistance : styles.distance}>
                                        <Text style={contextState.onGetDistance() === "5" ? {color: "#FFFFFF"} : {color: "#000000"}} >{"< 5km"}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={contextState.onSelectDistance.bind(this, "10")} style={contextState.onGetDistance() === "10" ? styles.selectedDistance : styles.distance}>
                                        <Text style={contextState.onGetDistance() === "10" ? {color: "#FFFFFF"} : {color: "#000000"}} >{"< 10km"}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.horizontalLine} />
                        </View>
                        { contextState.filterData.cost_catagory !== "" || contextState.filterData.distance !== "" || contextState.filterData.cuisinename.length !== 0 || contextState.filterData.typename.length !== 0 ? (
                                <TouchableOpacity style={styles.applyBtn} onPress={this.onApplyFilterHandler}>
                                    <Text style={styles.applyBtnText}>Apply Filters</Text>
                                </TouchableOpacity>)
                            : null}
                        { contextState.filterData.cost_catagory !== "" || contextState.filterData.distance !== "" || contextState.filterData.cuisinename.length !== 0 || contextState.filterData.typename.length !== 0 ? (
                                <TouchableOpacity style={styles.clearBtn} onPress={this.onClearFilterHandler}>
                                    <Text style={styles.applyBtnText}>Clear Filters</Text>
                                </TouchableOpacity>)
                            : null}
                    </ScrollView>
                )}
            </ContextConsumer>
        );
    }
}

const styles = StyleSheet.create({
    body:{
        flex: 1,
        backgroundColor: "#FFFFFF",
        flexDirection: "column",
    },
    horizontalLine: {
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E0',
        marginLeft: 17,
        marginRight: 17,
    },
    container:{
        margin: 16,
    },
    containerRow:{
        display: "flex",
        flexWrap: "wrap",
        flexDirection: "row",
    },
    title: {
        color: "#579041",
        fontSize: 20,
        marginBottom: 10,
        marginTop: 10,
        textTransform: "uppercase",
    },
    listItem:{
        marginTop: 10,
        marginBottom: 10,
        display: "flex",
        flexWrap: "wrap",
        flexDirection: "row",
    },
    costCatagory:{
        flex: 0.33,
        alignItems: "center",
        margin: 14,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#000000',
        height: 50,
        justifyContent: "center",
    },
    selectedCostCatagory: {
        flex: 0.33,
        alignItems: "center",
        margin: 14,
        borderRadius: 10,
        backgroundColor: "#579041",
        borderWidth: 1,
        borderColor: '#FFFFFF',
        height: 50,
        justifyContent: "center",
    },
    distance:{
        flex: 0.25,
        alignItems: "center",
        margin: 14,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#000000',
        height: 50,
        justifyContent: "center",
    },
    selectedDistance: {
        flex: 0.25,
        alignItems: "center",
        margin: 14,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        backgroundColor: "#579041",
        height: 50,
        justifyContent: "center",
    },
    applyBtn: {
        margin: 16,
        backgroundColor: "#579041",
        height: 50,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 5,
    },
    applyBtnText: {
        color: "#FFFFFF",
        fontWeight: "bold",
        fontSize: 16
    },
    clearBtn: {
        margin: 16,
        marginTop: 0,
        backgroundColor: "#FF0000",
        height: 50,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
    },
});


class FilterConsumer extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        headerLeft:
            (<TouchableOpacity style={{ marginLeft: 20 }} onPress={() => navigation.goBack()}>
                <MaterialIcon name="close" size={23} color='#E02222' />
            </TouchableOpacity>),
        title: "Filters",
    });

    render() {
        return (
            <ContextConsumer>
                {( contextState ) => (
                    <ListFilter contextState={contextState} navigation={this.props.navigation}/>
                )}
            </ContextConsumer>
        );
    }
}

export default FilterConsumer;