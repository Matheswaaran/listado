import { createStackNavigator } from "react-navigation";
import CollectionsTab from './CollectionsTab';
import CollectionsListTab from './CollectionsListTab';
import DetailedViewTab from './DetailedViewTab';
import ListFilter from './Filters/ListFilter';
import DietaryNeeds from "./Filters/DietaryNeeds";
import Cusines from "./Filters/Cusines";
import Settings from "./Settings";
import AboutApp from "./AboutApp";


export default createStackNavigator({
    Collections: {screen: CollectionsTab},
    CollectionsList: {screen: CollectionsListTab},
    DetailedView: {screen: DetailedViewTab},
    ListFilter: {screen: ListFilter},
    DietaryNeeds: {screen: DietaryNeeds},
    Cusines: {screen: Cusines},
    Settings: {screen: Settings},
    AboutApp: {screen: AboutApp}
});