import React from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity, Linking, Platform, Share } from "react-native";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ContextConsumer } from "./Context/Provider";
import CreateAlert from "./utils/CreateAlert";

class ListItem extends React.Component{
    constructor(props){
        super(props);
    }

    onLocationIconClickHandler = () => {
        const {lat, lang, name} = this.props.item;
        const URL = Platform.select({
            android: `geo:0,0?q=${name}@${lat},${lang}`,
            ios: `maps:0,0?=q=${name}@${lat},${lang}`,
        });
        Linking.openURL(URL);
    };

    shareIconClickHandler = () => {
        Share.share({
            title: this.props.item.name,
            message: `I found ${this.props.item.name} in Listado app. Check it out at http://www.listado.co.`,
        });
    };

    async componentDidMount() {

    }

    render() {
        const { item } = this.props;
        return(
            <View style={styles.container}>
                <View style={styles.image}>
                    <Image style={styles.imageView} source={{ uri: item.thumb_image }}/>
                </View>
                <View style={styles.texts}>
                    <View style={styles.wrap}>
                        <Text style={styles.title} numberOfLines={1}>{item.name}</Text>
                        {typeof this.props.mapFunction === 'undefined' ? null :
                            (<TouchableOpacity onPress={this.props.mapFunction} style={styles.close}>
                                <MCIcon name="close-circle-outline" size={23} color="#828282"/>
                            </TouchableOpacity>)
                        }
                    </View>
                    <Text style={styles.description} numberOfLines={1}>{item.byline}</Text>
                    <Text style={styles.description}>{item.display_name}</Text>
                    <View style={styles.wrap}>
                        <Text style={styles.open}>Open Now</Text>
                        <Text style={styles.amount}>$$$$</Text>
                    </View>
                    <View style={ styles.horizontalLine } />
                    <View style={styles.wrap}>
                        <ContextConsumer>
                            { (contextState) => (
                                <TouchableOpacity onPress={contextState.setFavourites.bind(this, item)} style={{ flex: 0.25, margin:6 }}>
                                    {contextState.isFavorite(item.id) ? (<MaterialIcon name="favorite" size={23} color="#E02222"/>) : (<MaterialIcon name="favorite-border" size={23} color="#828282"/>)}
                                </TouchableOpacity>
                            )}
                        </ContextConsumer>
                        <TouchableOpacity onPress={() => Linking.openURL(`tel:${item.phone_no}`)} style={{ flex: 0.25, margin:6 }}>
                            <MaterialIcon name="call" size={23} color="#828282"/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.onLocationIconClickHandler} style={{ flex: 0.25, margin:6 }}>
                            <MaterialIcon name="location-on" size={23} color="#828282"/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.shareIconClickHandler} style={{ flex: 0.25, margin:6 }}>
                            <MaterialIcon name="share" size={23} color="#828282"/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginLeft: 35,
        marginRight: 10,
        marginTop:7,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#DDD',
    },
    image: {
        flex: 0.3,
        position: 'relative',
        left: -20,
        right: 0,
        top: 10,
        height: 110,
        zIndex: 5,
    },
    imageView: {
        // width: 90,
        // height: 75,
        flex: 1,
        borderRadius: 10,
    },
    texts: {
        flex: 0.7,
    },
    title: {
        flex: 0.9,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontStyle: 'normal',
        fontWeight: 'bold',
        lineHeight: 21,
        fontSize: 16,
        marginTop: 8,
        marginBottom: 2,
        color: '#000000',
    },
    close: {
        flex: 0.1,
        padding: 5,
    },
    description: {
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        marginBottom: 2,
        textTransform: 'capitalize',
        color: '#828282',
    },
    open: {
        flex: 0.8,
        marginBottom: 2,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 19,
        fontSize: 14,
        color: '#219653',
    },
    amount: {
        flex: 0.2,
        color: '#F2994A',
    },
    wrap: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    horizontalLine: {
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E0',
        marginRight: 5,
        marginTop: 4,
        marginBottom: 4,
    },
});

export default ListItem;