import { createStackNavigator } from "react-navigation";
import DetailedViewTab from './DetailedViewTab';
import NearMeHome from './NearMeHome';
import MapFilter from './Filters/MapFilter';

export default createStackNavigator({
    NearMeHome: {screen: NearMeHome},
    NearMeDeatailedView: {screen: DetailedViewTab},
    NearMeFilter: {screen: MapFilter},
});