import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Dimensions, PermissionsAndroid, Platform, Picker} from 'react-native';
import Select from 'react-native-picker-select';
import CreateAlert from "./utils/CreateAlert";
import { GRAPHQL_API } from "./utils/URL";
import NearMeList from './NearMeList';
import NearMeMapView from './NearMeMapView';

class NearMeHome extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        ...Platform.select({ ios: {title: "NearMe"}, android: {headerLeft: (<Text style={styles.heading}>NearMe</Text>)} }),
    });

    constructor(props){
        super(props);
        this.state = { collections_id: '1', collections_name: [], selected: "maps"};
    }

    loadCollectionsName = async () => {
        try{
            let settings = {
                method: 'POST',
                headers: {
                    'deviceid': 'abc',
                    'Content-Type': 'application/json',
                    'cache-control': 'no-cache'
                },
                body: "{\"query\":\"{listCollections{id name}}\"}",
            };
            this.setState({refresh: true});
            const result = await fetch(GRAPHQL_API, settings);
            const response = await result.json();
            const data = Platform.OS === 'ios' ? response.data.listCollections.map(x => ({value: x.id, label: x.name })) : response.data.listCollections;
            this.setState({collections_name: data});
        }
        catch(e){
            console.error(e);
            // CreateAlert("", e);
        }
    };

    mapBtnClickHandler = () => {
        this.setState({ selected: "maps" });
    };

    listBtnClickHandler = () => {
        this.setState({ selected: "list" });
    };

    searchCollectionChangeHandler = (coll_id) => {
        this.setState({collections_id: coll_id});
    };

    _onCollectionsIDChangeHandler = (data) => {
        try {
            if (data !== ""){
                this.setState({ collections_id: data });
            }
        }catch (e) {
            console.error(e);
        }
    };


    async componentDidMount() {
        try {
            await this.loadCollectionsName();
        }catch (e) {
            console.error(e);
            // CreateAlert("", e);
        }
    }

    render(){
        return(
            <View style={styles.body}>
                <View style={styles.collectionsId_container}>
                    <Text style={styles.collectionsId} >Currently searching in </Text>
                    {Platform.OS === "android" ?
                        (<Picker style={styles.collectionsId_picker} selectedValue={this.state.collections_id} onValueChange={this.searchCollectionChangeHandler} mode='dropdown'>
                            {this.state.collections_name.map((collectionItem) => (
                                <Picker.Item label={collectionItem.name} value={collectionItem.id} key={collectionItem.id} style={styles.collectionsId_pickerItems}/>
                            ))}
                        </Picker>) :
                        (<Select value={this.state.collections_id}
                                 onValueChange={this._onCollectionsIDChangeHandler}
                                 items={this.state.collections_name}
                                 mode='dialog'
                                 style={styles.collectionsId_picker}/>)
                    }
                </View>
                <View style={styles.mapListBtn_container}>
                    <TouchableOpacity style={this.state.selected === 'maps' ? styles.mapListBtn_selected : styles.mapListBtn} onPress={this.mapBtnClickHandler}>
                        <Text style={{ color: this.state.selected === 'maps' ? "#FFFFFF" : "#579041" }}>Maps</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={this.state.selected === 'list' ? styles.mapListBtn_selected : styles.mapListBtn} onPress={this.listBtnClickHandler}>
                        <Text style={{ color: this.state.selected === 'list' ? "#FFFFFF" : "#579041" }}>List</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.mapView}>
                    {this.state.selected === "maps" ?
                        (<NearMeMapView collections_id={this.state.collections_id} navigation={this.props.navigation}/>)
                    :
                        (<NearMeList collections_id={this.state.collections_id} navigation={this.props.navigation}/>)
                    }
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    heading: {
        padding: 16,
        color: '#E02222',
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: '900',
    },
    settings: {
        marginRight: 20,
    },
    body:{
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
    },
    mapView: {
        flex: 0.9,
        height: Dimensions.get('screen').height,
        width: Dimensions.get('screen').width,
    },
    collectionsId_container: {
        flexDirection: 'row',
        backgroundColor: '#FFEEEE',
        height: 40,
        flex: 0.1,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    collectionsId: {
        margin: 10,
        marginLeft: 25,
        fontSize: 14,
        flex: 0.8,
    },
    collectionsId_picker: {
        flex: 0.2,
    },
    collectionsId_pickerItems: {
        flex: 1,
        margin: 10,
        marginLeft: 25,
        fontSize: 8,
    },
    mapListBtn_container: {
        flexDirection: 'row',
        height: 40,
        flex: 0.1,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mapListBtn: {
        height: 40,
        flex: 0.25,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#FFFFFF",
        borderWidth: 1,
        borderColor: "#579041",
    },
    mapListBtn_selected: {
        height: 40,
        flex: 0.25,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#579041",
        borderWidth: 1,
        borderColor: "#579041",
    },
});

export default NearMeHome;