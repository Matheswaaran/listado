import React from "react";
import { StyleSheet, TouchableOpacity, View, ScrollView, RefreshControl } from "react-native";
import ListItem from "./ListItem";
import { GRAPHQL_API } from "./utils/URL";
import CreateAlert from "./utils/CreateAlert";

class NearMeList extends React.Component {

    constructor(props) {
        super(props);
        this.state = { api_value: [], refresh: false};
    }

    loadApiData = async () => {
        try{
            const collections_id = this.props.collections_id;
            let settings = {
                method: 'POST',
                headers: {
                    'deviceid': 'abc',
                    'Content-Type': 'application/json',
                    'cache-control': 'no-cache'
                },
                body: "{\"query\":\"{list(collections_id:"+ `${collections_id}` +"){ id thumb_image name cost_categorization display_name byline lat lang phone_no }}\"}",
            };
            this.setState({refresh: true});
            const result = await fetch(GRAPHQL_API, settings);
            const response = await result.json();
            this.setState({api_value: response.data.list, refresh: false});
        }
        catch(e){
            console.error(e);
            CreateAlert("", e);
        }
    };

    _onScrollViewRefresh = async () => {
        try {
            this.setState({refresh: true});
            await this.loadApiData();
            this.setState({refresh: false});
        }catch(e){
            console.error(e);
            CreateAlert("", e);
        }
    };

    listItemPressHandler = (item_id, item_name) => {
        this.props.navigation.navigate('NearMeDeatailedView', {"item_id": item_id, "name": item_name});
    };

    async componentDidUpdate(prevProps) {
        try {
            if (this.props.collections_id !== prevProps.collections_id){
                this.setState({refresh: true});
                await this.loadApiData();
                this.setState({refresh: false});
            }
        }catch(e){
            console.error(e);
            CreateAlert("", e);
        }
    }


    async componentDidMount() {
        try {
            const res = await this.loadApiData();
        }catch (e) {
            console.error(e);
            CreateAlert("", e);
        }
    }

    render() {
        return (
            <View style={{ backgroundColor: "#FFFFFF", flex: 1 }}>
                <ScrollView style={styles.scrollview} refreshControl= {
                    <RefreshControl refreshing={this.state.refresh} onRefresh={this._onScrollViewRefresh} />
                }>
                    {this.state.api_value.map((item, index) => (
                        <TouchableOpacity onPress={this.listItemPressHandler.bind(this, item.id, item.name)} key={item.id}>
                            <ListItem key={item.id} item={item} />
                        </TouchableOpacity>
                    ))}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#FFFFFF',
        flex: 1,
    },
    body: {
        marginBottom: 15,
        marginTop: 10,
    },
});

export default NearMeList;