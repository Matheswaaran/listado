import React from 'react';
import {Text, View, StyleSheet, Dimensions, Platform, PermissionsAndroid, TouchableOpacity, Animated} from 'react-native';
import MapView, { Marker } from 'react-native-maps'
import { SEARCHFILTER_API } from "./utils/URL";
import CreateAlert from "./utils/CreateAlert";
import { region } from "./utils/Data";
import ListItem from './ListItem';

class NearMeMapView extends React.Component {

    constructor(props) {
        super(props);
        this.state = { api_value: [], region: {}, itemData: null };
    }

    onLoadApiData = async () => {
        const { collections_id } = this.props;
        const { latitude, longitude } = this.state.region;
        try{
            let settings = {
                method: 'GET',
                headers: { 'deviceid': 'abc', 'Content-Type': 'application/json'},
            };
            const result = await fetch(`${SEARCHFILTER_API}?collections_id=${collections_id}&lat=${latitude}&lang=${longitude}&distance=1`, settings);
            const response = await result.json();
            const apiData = response.lists.map(listitem => {
                const {id, name, byline, display_name, thumb_image, lat, lang, phone_no} = listitem;
                const stringID = id.toString();
                return {id: stringID, name, byline, display_name, thumb_image, lat, lang, phone_no};
            });
            this.setState({api_value: apiData,});
        }
        catch(e){
            console.error(e);
            // CreateAlert("", e);
        }
    };

    onMarkerPressHandler = (item) => {
        this.setState({ itemData: item });
    };

    onCardClickHandler = () => {
        const { itemData } = this.state;
        this.props.navigation.navigate('NearMeDeatailedView', {"item_id": itemData.id, "name": itemData.name});
    };


    getCurrentLocation = async () => {
    //     await navigator.geolocation.getCurrentPosition(position => this.setState({
    //         region: {
    //             latitude: parseFloat(position.coords.latitude),
    //             longitude: parseFloat(position.coords.longitude),
    //             latitudeDelta: 0.0922,
    //             longitudeDelta: 0.0421,
    //         }
    //     }));
    //     this.setState({ region: region[this.props.collections_id] });
    };

    onregionChangeHandler = async (region) => {
        try {
            this.setState({region: region});
            const result = await this.onLoadApiData();
        }catch (e) {
            console.error(e);
            // CreateAlert("", e);
        }
    };

    clearCardClickHandler = () => {
        this.setState({ itemData: null });
    };

    async componentDidUpdate(prevProps) {
        const { collections_id } = this.props;
        try {
            if (collections_id !== prevProps.collections_id && collections_id !== ""){
                this.setState({ region: region[collections_id], itemData: null });
                await this.onLoadApiData();
            }
        }catch(e){
            console.error(e);
            // CreateAlert("", e);
        }
    }

    async componentDidMount() {
        const { collections_id } = this.props;
        try {
            if (Platform.OS === "android"){
                const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
                if (granted === PermissionsAndroid.RESULTS.GRANTED){
                    this.setState({ region: region[collections_id] });
                    await this.onLoadApiData();
                } else{
                    CreateAlert("", "Permission Denied");
                }
            } else {
                await this.onLoadApiData();
            }
        }catch (e) {
            console.error(e);
            // CreateAlert("", e);
        }
    }

    render() {
        return (
            <View style={styles.body}>
                <MapView style={styles.mapView}
                         provider={"google"}
                         showsUserLocation={true}
                         showsMyLocationButton={true}
                         region={this.state.region}
                         onMapReady={this.getCurrentLocation}
                         onRegionChangeComplete={this.onregionChangeHandler}>
                    {this.state.api_value.map((item) => (
                        <Marker key={item.id} coordinate={{ latitude: parseFloat(item.lat), longitude: parseFloat(item.lang)}}
                                onPress={this.onMarkerPressHandler.bind(this, item)}/>
                    ))}
                </MapView>
                {this.state.itemData === {} || this.state.itemData === null ? null :
                    (<Animated.ScrollView style={styles.scrollView}
                                          pagingEnabled={true}
                                          scrollsToTop={false}
                                          scrollEventThrottle={100}
                                          removeClippedSubviews={true}
                                          automaticallyAdjustContentInsets={false}
                                          directionalLockEnabled={true}>
                        <TouchableOpacity onPress={this.onCardClickHandler}>
                            <ListItem item={this.state.itemData} style={styles.cardItem} mapFunction={this.clearCardClickHandler}/>
                        </TouchableOpacity>
                    </Animated.ScrollView>)
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    body:{
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
    },
    mapView: {
        flex: 1,
        height: Dimensions.get('screen').height,
        width: Dimensions.get('screen').width,
    },
    scrollView:{
        position: "absolute",
        bottom: 15,
        left: 0,
        right: 0,
        width: Dimensions.get('window').width,
    },
    cardItem: {
        // overflow: 'hidden',
        width: Dimensions.get('window').width,
    },
});

export default NearMeMapView;