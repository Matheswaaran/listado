import { createStackNavigator } from "react-navigation";
import DetailedViewTab from './DetailedViewTab';
import SearchScreen from "./SearchScreen";

export default createStackNavigator({
    Search: {screen: SearchScreen},
    SearchDetailedView: {screen: DetailedViewTab},
});