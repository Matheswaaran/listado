import React from "react";
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, RefreshControl, TextInput, Picker, Platform } from "react-native";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import debounce from "lodash/debounce";
import ListItem from "./ListItem";
import { GRAPHQL_API, SEARCHFILTER_API } from "./utils/URL";
import CreateAlert from "./utils/CreateAlert";
import Select from "react-native-picker-select";


class SearchScreen extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        ...Platform.select({ ios: {title: "Search"}, android: {headerLeft: (<Text style={styles.heading}>Search</Text>)} }),
    });

    constructor(props){
        super(props);
        this.state = {refresh: false, collections_id: "", search_value: "", collections_name: [], api_value: []};
    }

    loadCollectionsName = async () => {
        try{
            let settings = {
                method: 'POST',
                headers: {
                    'deviceid': 'abc',
                    'Content-Type': 'application/json',
                    'cache-control': 'no-cache'
                },
                body: "{\"query\":\"{listCollections{id name}}\"}",
            };
            this.setState({refresh: true});
            const result = await fetch(GRAPHQL_API, settings);
            const response = await result.json();
            const data = Platform.OS === 'ios' ? response.data.listCollections.map(x => ({value: x.id, label: x.name })) : response.data.listCollections;
            this.setState({collections_name: data, refresh: false});
        }
        catch(e){
            console.error(e);
            // CreateAlert("", e);
        }
    };

    loadApiData = async () => {
        const { collections_id, search_value } = this.state;
        try{
            let settings = {
                method: 'GET',
                headers: { 'deviceid': 'abc', 'Content-Type': 'application/json'},
            };
            this.setState({refresh: true});
            const result = await fetch(`${SEARCHFILTER_API}?collections_id=${collections_id}&searchtext=${search_value}`, settings);
            const response = await result.json();
            const apiData = response.lists.map(listitem => {
                const {id, name, byline, display_name, thumb_image, lat, lang, phone_no} = listitem;
                const stringID = id.toString();
                return {id: stringID, name, byline, display_name, thumb_image, lat, lang, phone_no};
            });
            this.setState({api_value: apiData, refresh: false});
        }
        catch(e){
            console.error(e);
            // CreateAlert("", e);
        }
    };

    _onScrollViewRefresh = async () => {
        try {
            this.setState({refresh: true});
            const res = await this.loadApiData();
            this.setState({refresh: false});
        }catch (e) {
            console.error(e);
            // CreateAlert("", e);
        }
    };

    onSearchInputChangeHandler = (text) => {
        if(this.state.collections_id !== "" && this.state.collections_id !== "0" ){
            this.setState({search_value: text}, debounce(this.loadApiData, 1000));
        }else{
            CreateAlert("", "Select a collection to search");
        }
    };

    listItemPressHandler = (item_id, item_name) => {
        const { navigation } = this.props;
        navigation.navigate('SearchDetailedView', {"item_id": item_id, "name": item_name});
    };

    searchCollectionChangeHandler = (coll_id) => {
        this.setState({collections_id: coll_id});
    };

    async componentDidMount() {
        try {
            const res = await this.loadCollectionsName();
        }catch (e) {
            console.error(e);
            // CreateAlert("", e);
        }
    }

    _onCollectionsIDChangeHandler = (data) => {
        this.setState({ collections_id: data });
    };

    render(){
        return(
            <View style={styles.body}>
                <View style={styles.collectionsId_container}>
                    <Text style={styles.collectionsId} >Currently searching in </Text>
                    {Platform.OS === "android" ?
                        (<Picker style={styles.collectionsId_picker} selectedValue={this.state.collections_id} onValueChange={this.searchCollectionChangeHandler} mode='dropdown'>
                            <Picker.Item label="-Select-" value="0" key={"0"} style={styles.collectionsId_pickerItems}/>
                            {this.state.collections_name.map((collectionItem) => (
                                <Picker.Item label={collectionItem.name} value={collectionItem.id} key={collectionItem.id} style={styles.collectionsId_pickerItems}/>
                            ))}
                        </Picker>) :
                        (<Select value={this.state.collections_id}
                                 onValueChange={this._onCollectionsIDChangeHandler}
                                 items={this.state.collections_name}
                                 mode='dialog'
                                 style={styles.collectionsId_picker}/>)
                    }
                </View>
                <View style={styles.searchContainer}>
                    <MaterialIcon name="search" size={25} color="#828282" style={styles.searchIcon}/>
                    <TextInput style={styles.searchInput} onChangeText={this.onSearchInputChangeHandler} autoFocus/>
                </View>
                <ScrollView refreshControl={
                    <RefreshControl refreshing={this.state.refresh} onRefresh={this._onScrollViewRefresh}/>
                }>
                {this.state.api_value.map((item, index) => {
                    return (
                    <TouchableOpacity onPress={this.listItemPressHandler.bind(this, item.id, item.name)} key={item.id}>
                        <ListItem key={item.id} item={item} />
                    </TouchableOpacity>
                )})}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    heading: {
        padding: 16,
        color: '#E02222',
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: '900',
    },
    settings: {
        marginRight: 20,
    },
    body: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        paddingTop: 10,
        paddingBottom: 15,
        flexDirection: 'column',
    },
    collectionsId_container: {
        flexDirection: 'row',
        backgroundColor: '#FFEEEE',
        height: 40,
    },
    collectionsId: {
        margin: 10,
        marginLeft: 25,
        fontSize: 14,
        flex: 0.8,
    },
    collectionsId_picker: {
        flex: 0.2,
    },
    collectionsId_pickerItems: {
        flex: 1,
        margin: 10,
        marginLeft: 25,
        fontSize: 8,
    },
    searchContainer: {
        flexDirection: 'row',
        borderColor: '#E0E0E0',
        borderWidth: 2,
        height: 40,
        margin: 16,
        borderRadius: 5,
    },
    searchIcon: {
        flex: 0.1,
        height: 40,
        padding: 6,
    },
    searchInput: {
        flex: 0.9,
        height: 40,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontSize: 14,
    },
});

export default SearchScreen;