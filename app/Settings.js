import React from 'react';
import {View, Text, StyleSheet, Platform, TouchableOpacity, Dimensions, Share, Linking} from 'react-native';
import MaterialIcon from "react-native-vector-icons/MaterialIcons";

class Settings extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        headerLeft:
            (<TouchableOpacity style={{ marginLeft: 20 }} onPress={() => navigation.goBack()}>
                <MaterialIcon name="chevron-left" size={23} color='#E02222' />
            </TouchableOpacity>),
        title: "Settings",
    });

    constructor(props) {
        super(props);
    }

    shareAppHandler = () => {
        Share.share({
            title: "Listado 1.0",
            message: "I found the Listado app. Check it out at http://www.listado.co.",
        });
    };

    onRatePressHandler = () => {
        const url = Platform.select({
            ios: "https://itunes.apple.com/in/app/eat-italy/id980192497?mt=8",
            android: "https://play.google.com/store/apps/details?id=com.ew.eatitaly",
        });
        Linking.openURL(url);
    };

    async componentDidMount() {

    }

    render() {
        return (
            <View style={styles.body}>
                <TouchableOpacity  onPress={this.shareAppHandler}>
                    <View style={styles.container}>
                        <MaterialIcon name="redo" size={25} color="#828282" style={styles.icon}/>
                        <Text style={styles.text}>Share the app</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.onRatePressHandler}>
                    <View style={styles.container}>
                        <MaterialIcon name="star" size={25} color="#828282" style={styles.icon}/>
                        <Text style={styles.text}>Rate the app</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("AboutApp")} >
                    <View style={styles.container}>
                        <MaterialIcon name="info" size={25} color="#828282" style={styles.icon}/>
                        <Text style={styles.text}>About the app</Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.about}>
                    <Text>Version 1.0.0</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    heading: {
        padding: 16,
        color: '#E02222',
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: '900',
    },
    body:{
        backgroundColor: "#FFFFFF",
        flex: 1,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexDirection: 'column',
    },
    container: {
        flexDirection: 'row',
        height: 40,
        margin: 16,
    },
    icon: {
        flex: 0.1,
        height: 40,
        padding: 6,
    },
    text: {
        flex: 0.9,
        height: 40,
        ...Platform.select({ android: {fontFamily : 'Lato'} }),
        fontSize: 16,
        margin: 7,
        color: "#333333",
    },
    about: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "flex-end",
        marginBottom: 20,
    },
});

export default Settings;