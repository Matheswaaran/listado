import { Platform, AlertIOS, ToastAndroid } from 'react-native';

function CreateAlert(title = "", message = ""){
	Platform.OS === "android" ? ToastAndroid.show(message, ToastAndroid.SHORT) : AlertIOS.alert(title, message);
}

export default CreateAlert;