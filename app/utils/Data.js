const region = {
    "1" : {latitude: 43.7696, longitude: 11.2558, latitudeDelta: 0.005088616980401639, longitudeDelta: 0.004248954355718482},
    "2" : {latitude: 41.9028, longitude: 12.4964, latitudeDelta: 0.005088616980401639, longitudeDelta: 0.004248954355718482},
    "3" : {latitude: 40.7928, longitude: 17.1012, latitudeDelta: 0.005088616980401639, longitudeDelta: 0.004248954355718482},
    "6" : {latitude: 42.9380, longitude: 12.6216, latitudeDelta: 0.005088616980401639, longitudeDelta: 0.004248954355718482},
    "7" : {latitude: 45.4642, longitude: 9.1900, latitudeDelta: 0.005088616980401639, longitudeDelta: 0.004248954355718482},
    "8" : {latitude: 45.0703, longitude: 7.6869, latitudeDelta: 0.005088616980401639, longitudeDelta: 0.004248954355718482},
    "9" : {latitude: 45.44087, longitude: 12.3285, latitudeDelta: 0.005088616980401639, longitudeDelta: 0.004248954355718482},
};

export { region };