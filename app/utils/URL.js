const GRAPHQL_API = "http://api.listado.co/graphql";
const SEARCHFILTER_API = "http://api.listado.co/searchfilter";
const ASYNC_STORAGE_NAME = "FAV_ITEM_IDS";
const NO_IMAGE_AVAILABLE = "https://media-dirtboundoffroad.netdna-ssl.com/pub/media/catalog/product/placeholder/default/no_image_available_3.jpg";
const GET_TYPE_API = "http://api.listado.co/gettype";
const GET_CUSINE_API = "http://api.listado.co/getcusine";
const GET_AMBIENCE_API = "http://api.listado.co/getambience";

export { GRAPHQL_API, SEARCHFILTER_API, ASYNC_STORAGE_NAME, NO_IMAGE_AVAILABLE, GET_TYPE_API, GET_AMBIENCE_API, GET_CUSINE_API };