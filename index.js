/** @format */
import React  from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { ContextProvider } from "./app/Context/Provider";

class Listado extends React.Component {
    render() {
        return (
            <ContextProvider>
                <App />
            </ContextProvider>
        );
    }
}

AppRegistry.registerComponent(appName, () => Listado);
